package hellopackage;
import secondpackage.Utilities;
import java.util.Scanner;


public class Greeter {
    public static void main (String[] args) {
        Scanner reader = new Scanner(System.in);
       
        System.out.println("Enter any number :");
        int temp = reader.nextInt();
        Utilities u = new Utilities();
        int newTemp = u.doubleMe(temp);
        System.out.println("Your new number is : "+newTemp);
    }

}

